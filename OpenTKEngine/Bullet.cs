﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;
using OpenTK.Input;

namespace OpenTKEngine
{
	class Bullet
	{
		public Vector2 position, velocity;
		public float radius;
		public bool alive;

		public Bullet(Vector2 position, Vector2 velocity, float radius)
		{
			this.position = position;
			this.velocity = velocity;
			this.radius = radius;
			alive = true;
		}

		public void Update()
		{
			position += velocity;
			if (position.X < 0 - radius)
				alive = false;
			if (position.Y < 0 - radius)
				alive = false;
			if (position.X > Client.scrSize.X + radius)
				alive = false;
			if (position.Y > Client.scrSize.Y + radius)
				alive = false;
		}

		public void Draw()
		{
			Client.DrawCircle(position, radius, 7, System.Drawing.Color.White);
			Vector3 d1 = Vector3.Cross(new Vector3(velocity.X, velocity.Y, 0), new Vector3(0, 0, -1));
			Vector3 d2 = Vector3.Cross(new Vector3(velocity.X, velocity.Y, 0), new Vector3(0, 0, 1));
			d1.Normalize();
			d2.Normalize();
			Vector2 p1 = position + new Vector2(d1.X * radius, d1.Y * radius);
			Vector2 p2 = position + new Vector2(d2.X * radius, d2.Y * radius);
			Vector2 p3 = position - Vector2.Normalize(velocity) * radius - velocity * 2.5f;
			Client.DrawLine(p1, p3, System.Drawing.Color.White);
			Client.DrawLine(p2, p3, System.Drawing.Color.White);

		}
	}
}
