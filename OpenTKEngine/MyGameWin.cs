﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using OpenTK.Input;
using System.Drawing;
using System.Drawing.Imaging;

namespace OpenTKEngine
{
	class MyGameWin : GameWindow
	{
		public static Vector2 mousePos;

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);

			Title = "OpenTKEngine1.0";

			GL.ClearColor(Color.Black);

			Client.LoadContent(new Vector2(ClientRectangle.Width, ClientRectangle.Height));
		}

		protected override void OnUpdateFrame(FrameEventArgs e)
		{
			base.OnUpdateFrame(e);
			mousePos = new Vector2(this.Mouse.X, this.Mouse.Y);
			
			Client.Update();
		}

		protected override void OnRenderFrame(FrameEventArgs e)
		{
			base.OnRenderFrame(e);
			GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

			//Matrix4 modelview = Matrix4.LookAt(Vector3.Zero, Vector3.UnitZ, Vector3.UnitY);

			//GL.MatrixMode(MatrixMode.Modelview);
			//GL.LoadMatrix(ref modelview);

			//This is my actual draw method stuff
			Console.WriteLine(Math.Round(RenderFrequency, 1));
			GL.Begin(PrimitiveType.Lines);

			Client.Draw();

			GL.End();
			//End of draw method stuff

			SwapBuffers();
		}

		protected override void OnResize(EventArgs e)
		{
			base.OnResize(e);

			//GL.Viewport(ClientRectangle.X, ClientRectangle.Y, ClientRectangle.Width, ClientRectangle.Height);

			Matrix4 projection = Matrix4.CreatePerspectiveFieldOfView((float)Math.PI / 4, Width / (float)Height, 1.0f, 64.0f);

			//GL.MatrixMode(MatrixMode.Projection);
			//GL.LoadMatrix(ref projection);
		}

		static int LoadTexture(string filename)
		{
			if (String.IsNullOrEmpty(filename))
				throw new ArgumentException(filename);

			int id = GL.GenTexture();
			GL.BindTexture(TextureTarget.Texture2D, id);

			Bitmap bmp = new Bitmap(filename);
			BitmapData bmpData = bmp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height), ImageLockMode.ReadOnly, System.Drawing.Imaging.PixelFormat.Format32bppArgb);

			GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba, bmpData.Width, bmpData.Height, 0, OpenTK.Graphics.OpenGL.PixelFormat.Bgra, PixelType.UnsignedByte, bmpData.Scan0);

			bmp.UnlockBits(bmpData);

			//Disable bitmaps for now since we didn't specify any
			GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Linear);
			GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Linear);

			return id;
		}
	}
}
