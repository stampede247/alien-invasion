﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;
using OpenTK.Input;

namespace OpenTKEngine
{
	class Enemy
	{
		public Vector2 position;
		public float radius, speed, magnitude;
		public bool alive;
		private int t;

		public Enemy(Vector2 position, float speed, float magnitude, float radius)
		{
			this.position = position;
			this.speed = speed;
			this.magnitude = magnitude;
			this.radius = radius;
			alive = true;
			t = 0;
		}

		public void Update()
		{
			if (position.X < 0 - radius)
				alive = false;
			if (position.Y < 0 - radius)
				alive = false;
			if (position.X > Client.scrSize.X + radius)
				alive = false;
			if (position.Y > Client.scrSize.Y + radius)
				alive = false;

			t++;
			position.X += speed;
			position.Y += (float)Math.Cos(t * (Math.PI / 8)) * magnitude;
		}

		public void Draw()
		{
			Client.DrawCirclePart(position, radius / 2, 14, System.Drawing.Color.LimeGreen, (float)Math.PI, (float)Math.PI * 2);
			Client.DrawOval(position + new Vector2(0, radius / 4), new Vector2(radius, radius / 3), 20, System.Drawing.Color.Green);
		}
	}
}
