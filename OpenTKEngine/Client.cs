﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using OpenTK.Input;
using System.Drawing;
using System.Drawing.Imaging;

namespace OpenTKEngine
{
	class Client
	{
		public static Vector2 scrSize;
		public static MouseState mCurrent, mLast;
		public static KeyboardState kCurrent, kLast;

		public static List<Bullet> bullets;
		public static Turret turret;
		public static List<Enemy> enemies;
		public static List<Particle> particles;
		public static Random rand;
		public static float chanceOfSpawn = 0.98f;

		public static void LoadContent(Vector2 screenSize)
		{
			scrSize = screenSize;

			bullets = new List<Bullet>();
			enemies = new List<Enemy>();
			particles = new List<Particle>();
			turret = new Turret(new Vector2(640, 480));
			rand = new Random();
		}

		public static void Update()
		{
			mLast = mCurrent;
			mCurrent = Mouse.GetState();
			kLast = kCurrent;
			kCurrent = Keyboard.GetState();

			if (MouseDown(false))
			{
				
			}

			//Generate enemies randomly
			if (rand.NextDouble() > chanceOfSpawn)
			{
				CreateEnemy(new Vector2(-10, Lerp(10, 360, (float)rand.NextDouble())));
				chanceOfSpawn -= 0.0005f;
			}

			#region Update Lists
			turret.Update();
			for (int i = 0; i < particles.Count; i++)
			{
				if (particles[i].alive)
				{
					particles[i].Update();
				}
			}
			for (int i = 0; i < enemies.Count; i++)
			{
				if (enemies[i].alive)
				{
					enemies[i].Update();
				}
			}
			for (int i = 0; i < bullets.Count; i++)
			{
				if (bullets[i].alive)
				{
					bullets[i].Update();

					for (int i2 = 0; i2 < enemies.Count; i2++)
					{
						if (enemies[i2].alive)
						{

							if (Math.Pow(enemies[i2].position.X - bullets[i].position.X, 2) +
								Math.Pow(enemies[i2].position.Y - bullets[i].position.Y, 2) <
								Math.Pow(enemies[i2].radius + bullets[i].radius, 2))
							{
								bullets[i].alive = false;
								enemies[i2].alive = false;
								CreateExplosion(enemies[i2].position, enemies[i2].radius / 2, -(float)Math.Atan2(bullets[i].velocity.Y, bullets[i].velocity.X), 30);
							}
						}
						else
						{
							//remove the dead enemy
							enemies.RemoveAt(i2);
							i2--;
						}
					}
				}
				else
				{
					//Remove the dead bullet
					bullets.RemoveAt(i);
					i--;
				}
			}
			#endregion
		}

		public static void CreateBullet(Vector2 position, Vector2 velocity, float radius)
		{
			bullets.Add(new Bullet(position, velocity, radius));
		}
		public static void CreateEnemy(Vector2 position)
		{
			enemies.Add(new Enemy(position, (float)rand.NextDouble() * 1.5f + 2, (float)rand.NextDouble() * 0.5f + 1, 20));
		}
		public static void CreateExplosion(Vector2 position, float radius, float direction, int numParticles)
		{
			float varDir = MathHelper.Pi;
			for (int i = 0; i < numParticles; i++)
			{
				float distance = Lerp(0, radius, (float)rand.NextDouble());
				float disDirection = Lerp(0, (float)Math.PI * 2, (float)rand.NextDouble());
				Vector2 pos = position + new Vector2((float)Math.Cos(disDirection) * distance, (float)Math.Sin(disDirection) * distance);
				float dir = Lerp(direction - varDir, direction + varDir, (float)rand.NextDouble());
				float size = Lerp(0.5f, 7f, (float)rand.NextDouble());
				float speed = Lerp(1f, 2f, (float)rand.NextDouble());
				int life = (int)Lerp(20, 40, (float)rand.NextDouble());
				Color color = (rand.NextDouble() < 0.2) ? Color.Red : Color.Green;
				particles.Add(new Particle(pos, new Vector2((float)Math.Cos(dir) * speed, (float)Math.Sin(dir) * speed), size, color, life));
			}
		}


		public static void Draw()
		{
			turret.Draw();
			for (int i = 0; i < particles.Count; i++)
			{
				if (particles[i].alive)
				{
					particles[i].Draw();
				}
			}
			for (int i = 0; i < bullets.Count; i++)
			{
				if (bullets[i].alive)
				{
					bullets[i].Draw();
				}
			}
			for (int i = 0; i < enemies.Count; i++)
			{

				if (enemies[i].alive)
				{
					enemies[i].Draw();
				}
			}
		}

		public static void DrawCircle(Vector2 center, float radius, int numVertices, Color color)
		{
			for (int i = 0; i < numVertices; i++)
			{
				float dir1 = (float)((Math.PI * 2) / numVertices * i);
				float dir2 = (float)((Math.PI * 2) / numVertices * (i + 1));

				DrawLine(new Vector2((float)Math.Cos(dir1) * radius + center.X, (float)Math.Sin(dir1) * radius + center.Y),
					new Vector2((float)Math.Cos(dir2) * radius + center.X, (float)Math.Sin(dir2) * radius + center.Y),
					color);
			}
		}
		public static void DrawCirclePart(Vector2 center, float radius, int numVertices, Color color, float minDir, float maxDir)
		{
			for (int i = 0; i < numVertices; i++)
			{
				float dir1 = (float)((maxDir - minDir) / numVertices * i + minDir);
				float dir2 = (float)((maxDir - minDir) / numVertices * (i + 1) + minDir);

				DrawLine(new Vector2((float)Math.Cos(dir1) * radius + center.X, (float)Math.Sin(dir1) * radius + center.Y),
					new Vector2((float)Math.Cos(dir2) * radius + center.X, (float)Math.Sin(dir2) * radius + center.Y),
					color);
			}
		}
		public static void DrawOval(Vector2 center, Vector2 radius, int numVertices, Color color)
		{
			for (int i = 0; i < numVertices; i++)
			{
				float dir1 = (float)((Math.PI * 2) / numVertices * i);
				float dir2 = (float)((Math.PI * 2) / numVertices * (i + 1));

				DrawLine(new Vector2((float)Math.Cos(dir1) * radius.X + center.X, (float)Math.Sin(dir1) * radius.Y + center.Y),
					new Vector2((float)Math.Cos(dir2) * radius.X + center.X, (float)Math.Sin(dir2) * radius.Y + center.Y),
					color);
			}
		}
		///P2   Vy  P1
		/// *---\---*
		///  \   \   \
		///   \  C*----Vx
		///    \       \
		///     *-------*
		///     P3      P4
		/// <param name="center">center point of the box on the screen</param>
		/// <param name="size">full width and height from end to end</param>
		/// <param name="rotation">rotation of the box around the origin in radians</param>
		/// <param name="thickness">thickness of the lines representing the box</param>
		/// <param name="color">color or the lines representing the box</param>
		public static void DrawBox(Vector2 center, Vector2 size, float rotation, Color color)
		{
			Vector2 p1, p2, p3, p4;
			Vector2 Vx, Vy;
			Vx = new Vector2((float)Math.Cos(rotation), (float)Math.Sin(rotation));
			Vy = new Vector2((float)Math.Cos(rotation + Math.PI / 2), (float)Math.Sin(rotation + Math.PI / 2));
			p1 = center + Vx * (size.X / 2) + Vy * (size.Y / 2);
			p2 = center + -Vx * (size.X / 2) + Vy * (size.Y / 2);
			p3 = center + -Vx * (size.X / 2) + -Vy * (size.Y / 2);
			p4 = center + Vx * (size.X / 2) + -Vy * (size.Y / 2);

			DrawLine(p1, p2, color);
			DrawLine(p2, p3, color);
			DrawLine(p3, p4, color);
			DrawLine(p4, p1, color);
		}
		public static void DrawLine(Vector2 p1, Vector2 p2, Color color)
		{
			GL.Color4(color);
			GL.Vertex2(ConvertToScreenCoords(p1));
			GL.Vertex2(ConvertToScreenCoords(p2));
		}
		public static Vector2 ConvertToScreenCoords(Vector2 p)
		{
			return new Vector2((p.X - scrSize.X / 2) / (scrSize.X / 2), -(p.Y - scrSize.Y / 2) / (scrSize.Y / 2));
		}

		public static bool MousePressed(bool left)
		{
			if (left)
			{
				return (mCurrent.LeftButton == ButtonState.Pressed && mLast.LeftButton == ButtonState.Released);
			}
			else
			{
				return (mCurrent.RightButton == ButtonState.Pressed && mLast.RightButton == ButtonState.Released);
			}
		}
		public static bool MouseDown(bool left)
		{
			if (left)
			{
				return (mCurrent.LeftButton == ButtonState.Pressed);
			}
			else
			{
				return (mCurrent.RightButton == ButtonState.Pressed);
			}
		}
		public static bool KeyPressed(Key key)
		{
			return (kCurrent.IsKeyDown(key) && kLast.IsKeyUp(key));
		}
		public static bool KeyDown(Key key)
		{
			return (kCurrent.IsKeyDown(key));
		}

		public static float Lerp(float min, float max, float value)
		{
			return (min + (max - min) * value);
		}
	}
}
