﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenTKEngine
{
	class Program
	{
		static void Main(string[] args)
		{
			using (MyGameWin game = new MyGameWin())
			{
				game.Run(60);
			}
		}
	}
}
