﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;
using OpenTK.Input;
using System.Drawing;

namespace OpenTKEngine
{
	class Particle
	{
		public const float GRAVITY = 0.0f;
		public Vector2 position, velocity;
		public float radius;
		public bool alive;
		public Color color;
		public int lifeSpan, lifeCurrent;

		private float LifeCurrentPercent
		{
			get
			{
				return (float)lifeCurrent / (float)lifeSpan;
			}
		}

		public Particle(Vector2 position, Vector2 velocity, float radius, Color color, int lifeSpan)
		{
			this.position = position;
			this.velocity = velocity;
			this.radius = radius;
			this.alive = true;
			this.lifeSpan = lifeSpan;
			this.lifeCurrent = lifeSpan;
			this.color = color;

		}

		public void Update()
		{
			velocity.Y += GRAVITY;
			position += velocity;
			lifeSpan--;
			if (lifeSpan < 0)
			{
				alive = false;
				return;
			}
			if (position.X < 0 - radius)
				alive = false;
			if (position.Y < 0 - radius)
				alive = false;
			if (position.X > Client.scrSize.X + radius)
				alive = false;
			if (position.Y > Client.scrSize.Y - radius)
			{
				position.Y = Client.scrSize.Y - radius;
				velocity.Y = -velocity.Y * 0.4f;
			}
		}

		public void Draw()
		{
			Client.DrawCircle(position, radius, 7, color);
			Vector3 d1 = Vector3.Cross(new Vector3(velocity.X, velocity.Y, 0), new Vector3(0, 0, -1));
			Vector3 d2 = Vector3.Cross(new Vector3(velocity.X, velocity.Y, 0), new Vector3(0, 0, 1));
			d1.Normalize();
			d2.Normalize();
			Vector2 p1 = position + new Vector2(d1.X * radius, d1.Y * radius);
			Vector2 p2 = position + new Vector2(d2.X * radius, d2.Y * radius);
			Vector2 p3 = position - Vector2.Normalize(velocity) * radius - velocity * 2.5f;
			Client.DrawLine(p1, p3, color);
			Client.DrawLine(p2, p3, color);

		}
	}
}
