﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;
using OpenTK.Input;
using System.Drawing;

namespace OpenTKEngine
{
	class Turret
	{
		public Vector2 position;
		public float rotation;

		public Turret(Vector2 position)
		{
			this.position = position;
			this.rotation = 0;
		}

		public void Update()
		{
			rotation = (float)Math.Atan2(MyGameWin.mousePos.Y - position.Y, MyGameWin.mousePos.X - position.X);
			//if (rotation < Math.PI / 2)
			//	rotation = (float)(Math.PI / 2);
			//if (rotation > Math.PI)
			//	rotation = (float)(Math.PI);

			if (Client.MousePressed(true))
			{
				float bulletSpeed = 14f;
				Client.CreateBullet(position, new Vector2((float)Math.Cos(rotation) * bulletSpeed, (float)Math.Sin(rotation) * bulletSpeed), 2.5f);
			}
		}

		public void Draw()
		{
			Client.DrawBox(position, new Vector2(40, 10), rotation, Color.White);
		}
	}
}
